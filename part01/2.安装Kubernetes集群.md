# 安装K8S集群



## 前言


在K8S中，是使用集群来组织服务器的。集群中会存在一个  `Master` 节点，该节点是K8S集群的控制节点，负责调度集群中其他服务器的资源。其他节点被称为 `Node` ， `Node` 可以是物理机也可以是虚拟机。

## 基础安装


无论是  `Master` 还是 `Node` 节点，都需要安装一些必备依赖组件


### 安装必备依赖
```shell
yum install vim wget -y
```


### 解决 raw.githubusercontent.com 无法访问问题


编辑 `hosts` 文件，直接指定其IP访问即可。
如果该IP失效，也可以去 [https://githubusercontent.com.ipaddress.com/raw.githubusercontent.com](https://githubusercontent.com.ipaddress.com/raw.githubusercontent.com) 获取新的IP：
```shell
sudo vi /etc/hosts
```


在最后一行写入：
```
199.232.4.133 raw.githubusercontent.com
```
### 
### 关闭防火墙


需要关闭防火墙，确保可以自由访问端口
```shell
systemctl stop firewalld & systemctl disable firewalld
```


### 关闭 swap
```shell
#临时关闭
swapoff -a
# 永久关闭
vi /etc/fstab
```


注释以下代码
```shell
/dev/mapper/centos-swap swap ...
```


### 关闭 selinux
```shell
# 暂时关闭 selinux
setenforce 0
# 永久关闭 需重启
vi /etc/sysconfig/selinux
# 修改以下参数，设置为disable
SELINUX=disabled
```
**为什么关闭防火墙，selinux，swap？可以看这里：**[**https://www.zhihu.com/question/374752553**](https://www.zhihu.com/question/374752553)
### 修改网络配置
```shell
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
```


### 统一时间和时区
```shell
# 统一时区，为上海时区
ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
bash -c "echo 'Asia/Shanghai' > /etc/timezone"

# 统一使用阿里服务器进行时间更新
yum install -y ntpdate
ntpdate ntp1.aliyun.com
```


### 安装 docker

参考链接:[安装docker](https://gitee.com/zhufengpeixun/zhifengpro-cicd/blob/master/day02/%E7%AC%AC%E4%BA%8C%E5%A4%A9%E7%9A%84%E8%AF%BE%E9%A2%98_2.md)

### 安装K8S组件


#### 1. 更新K8S源


更换K8S源为国内阿里云源
```shell
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
        http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```


#### 2. 安装kubelet、kubeadm、kubectl
```shell
yum install -y kubelet kubeadm kubectl
# 启动kubelet
systemctl enable kubelet && systemctl start kubelet
```



---



## Master 节点安装


在 `master` 机器上执行：
```shell
hostnamectl set-hostname  master
```


使用 `ip addr` 命令，获取本机IP，将其添加到 `/etc/hosts` 内：
```shell
# xxx.xxx.xxx.xxx master
vim /etc/hosts
```


### 配置K8S初始化文件


首先，我们使用 `kubeadm config print init-defaults` 输出一份默认配置，使用 `>` 操作符导出为一份文件，方便我们进行修改
```shell
kubeadm config print init-defaults > init-kubeadm.conf
vim init-kubeadm.conf
```


主要对配置文件做这几件事情：

- 更换k8s镜像仓库为阿里云镜像仓库，加速组件拉取
- 替换ip为自己主机ip
- 配置pod网络为 flannel 网络
```shell
# imageRepository: k8s.gcr.io 更换k8s镜像仓库
imageRepository: registry.cn-hangzhou.aliyuncs.com/google_containers
# localAPIEndpointc，advertiseAddress为master-ip ，port默认不修改
localAPIEndpoint:
  advertiseAddress: 192.168.56.101  # 此处为master的IP
  bindPort: 6443
# 配置子网络
networking:
  dnsDomain: cluster.local
  serviceSubnet: 10.96.0.0/12
  podSubnet: 10.244.0.0/16	# 添加这个
```


### 拉取镜像


直接使用 `kubeadm config images pull` 拉取k8s组件镜像
```shell
kubeadm config images pull --config init-kubeadm.conf
```


### 初始化K8S


刚才我们生成修改了一份初始化配置文件，可以直接使用该配置文件初始化k8s：
```shell
kubeadm init --config init-kubeadm.conf
```
在静等一会后，终端会给出以下提示，按照提示执行：


![last_runtime](https://images.gitee.com/uploads/images/2020/1226/200801_bbc09471_1720749.png "last_runtime.png")


其中，红框命令为在master节点需要执行的初始化命令。蓝框为需要稍后在node节点执行的 `加入master` 命令。


### 安装 flannel


flannel 作为k8s的一个组件，则使用k8s启动容器的方式安装。首先下载配置文件：
```shell
wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```


接着手动拉取下 `flannel` 镜像。找到编写镜像名称和版本的地方，手动 `pull` 一下：


![last_flannel](https://images.gitee.com/uploads/images/2020/1226/200719_d864c9ac_1720749.png "last_flannel.png")

```shell
docker pull quay.io/coreos/flannel:v0.13.0-rc2
```


等待镜像拉取结束后，使用 `kubectl apply` 命令载入下
```shell
kubectl apply -f kube-flannel.yml
```


### 查看启动情况


我们可以使用 `kubectl get nodes` 命令查看 `node` 节点（包括 master）状态。如果 `STATUS = ready`，则代表启动成功。


```shell
kubectl get nodes
```

![last_node](https://images.gitee.com/uploads/images/2020/1226/200549_ef3e9fc8_1720749.png "last_node.png")




## Node节点配置


在 `node` 机器上执行：
```shell
hostnamectl set-hostname  node1
```


### 拷贝 master 节点配置文件


首先先去 `master` 节点拷贝 `k8s` 的配置文件到 `node` 节点。在 `master` 节点执行：
```shell
scp $HOME/.kube/config root@node的ip:~/
```


随后在 `node` 节点执行以下命令，归档配置文件：
```shell
mkdir -p $HOME/.kube
sudo mv $HOME/config $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```


### 加入 master 节点


随后，我们使用刚才在master生成的节点加入命令，在node机器上执行，加入到 `master` 集群内：
```shell
# 这条是示例命令！！！！！！
kubeadm join 172.16.81.164:6443 --token abcdef.0123456789abcdef \
    --discovery-token-ca-cert-hash sha256:b4a059eeffa2e52f2eea7a5d592be10c994c7715c17bda57bbc3757d4f13903d
```


如果刚才的命令丢了，可以在master机器上重新生成命令：
```shell
kubeadm token create --print-join-command
```

![last_0](https://images.gitee.com/uploads/images/2020/1226/200213_3dd834e9_1720749.png "last-0.png")


### 安装 flannel


**这里和 master 安装方式一样，参考同上。**


